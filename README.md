# Mark Payment SDK For PHP

[![Latest Stable Version](https://poser.pugx.org/tianfuunion/mark-payment/v/stable)](https://packagist.org/packages/tianfuunion/mark-payment)
[![Build Status](https://travis-ci.org/tianfuunion/mark-payment.svg?branch=master)](https://travis-ci.org/tianfuunion/mark-payment)
[![Coverage Status](https://coveralls.io/repos/github/tianfuunion/mark-payment/badge.svg?branch=master)](https://coveralls.io/github/tianfuunion/mark-payment?branch=master)

## 概述

{聚合支付（Mark Payment），让支付更简单！聚合支付平台，主要提供支付接口技术服务！一次对接，便可使用多支付方式，同时管理免签通道和签约通道的支付平台收款账号。详情请看 [https://pay.tianfu.ink](https://pay.tianfu.ink)}

## 软件架构

软件架构说明

## 运行环境

- PHP 7.2+
- cURL extension

## 安装方法

如果您通过composer管理您的项目依赖，可以在你的项目根目录运行：

        $ composer require tianfuunion/mark-payment

或者在你的`composer.json`中声明对 Mark Payment SDK For PHP 的依赖：

        "require": {
            "tianfuunion/mark-payment": "~2.0.*"
        }

然后通过`composer install`安装依赖。composer安装完成后，在您的PHP代码中引入依赖即可：

        require_once __DIR__ . '/vendor/autoload.php';

## 使用说明

1. xxxx
2. xxxx
3. xxxx

## 版权信息

Mark Authorize SDK For PHP遵循MulanPSL-2.0开源协议发布，并提供免费使用。

本项目包含的第三方源码和二进制文件之版权信息另行标注。

版权所有 南阳市天府网络科技有限公司

Copyright © 2017-2024 by TianFuUnion (https://www.tianfuunion.cn) All rights reserved。

TianFuUnion® 商标和著作权所有者为南阳市天府网络科技有限公司。

- [MulanPSL-2.0](http://license.coscl.org.cn/MulanPSL2/)
- 更多细节参阅 [LICENSE.txt](LICENSE.txt)

## 联系我们

- [天府联盟官方网站：www.tianfuunion.cn](https://www.tianfuunion.cn)
- [天府授权中心官方网站：auth.tianfu.ink](https://auth.tianfu.ink)
- [天府联盟反馈邮箱：report@tianfuunion.cn](mailto:report@tianfuunion.cn)