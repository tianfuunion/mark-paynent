<?php
declare (strict_types=1);

namespace mark\payment;

/**
 * Class Payment
 *
 * @modifyTime   2022:02:13 10:24:00
 * @author       Mark Young
 * @version      0.0.2
 * @description  聚合支付基础类
 * @package      mark\payment
 */
class Payment {

    protected $appid;
    protected $sp_appid;

    protected $corpid;
    protected $sp_corpid;

    protected $mchid;
    protected $sp_mchid;

    protected $secret;
    protected $ticket;

    protected $serial_no;
    protected $private_key;
    protected $certificate;

    public $protocol = 'https';
    public static $host = 'https://pay.tianfu.ink';
    public $gatewayHost = 'pay.tianfu.ink';
    public $signType;
    public $publicKey;
    public $merchantPrivateKey;
    public $merchantCertPath;
    public $certPath;
    public $rootCertPath;
    public $merchantCertSN;
    public $certSN;
    public $rootCertSN;
    public $notifyUrl;
    public $encryptKey;
    public $httpProxy;
    public $ignoreSSL;

    /**
     * @var \mark\payment\kernel\Kernel
     */
    public $kernel;

    /**
     * @var \mark\payment\Payment
     */
    private static $instance;

    /**
     * @var \mark\payment\Merchant
     */
    protected static $merchant;

    /**
     * @var \mark\payment\Trade
     */
    protected static $trade;

    /**
     * @var \mark\payment\Refund
     */
    protected static $refund;

    /**
     * @var \mark\payment\Settlement
     */
    protected static $settlement;

    /**
     * @var \mark\payment\Partner
     */
    protected static $partner;

    public static $_type = 'mark-payment';
    public static $_version = '1.0.0';

    /**
     * 支付渠道列表
     *
     * @var array
     */
    public static $pay_channel = array(
        0 => array('title' => '其它支付', 'name' => 'OtherPay', 'value' => 0, 'icon' => 'icon-card-pos', 'describe' => '第三方支付，未知支付'),
        1 => array('title' => '微信支付', 'name' => 'WeChatPay', 'value' => 1, 'icon' => 'icon-wechat-pay', 'describe' => '微信支付', 'checked' => true),
        2 => array('title' => '支付宝', 'name' => 'AliPayWap', 'value' => 2, 'icon' => 'icon-alipay', 'describe' => '支付宝支付'),
        3 => array('title' => '零钱支付', 'name' => 'WalletPay', 'value' => 3, 'icon' => 'icon-wallet-pay', 'describe' => '零钱支付、钱包支付'),
        4 => array('title' => '银联支付', 'name' => 'UnionPay', 'value' => 4, 'icon' => 'icon-union-pay', 'describe' => '银联支付')
    );

    /**
     * 获取支付渠道
     *
     * @param $channel
     *
     * @return array
     */
    public static function getPayChannel($channel): array {
        if (array_key_exists($channel, self::$pay_channel)) {
            return self::$pay_channel[$channel] ?? array();
        }

        return self::$pay_channel[array_key_exists($channel, self::$pay_channel) ? $channel : 0];
    }

    /**
     * Payment constructor.
     *
     * @param array $config
     */
    private function __construct(array $config) {
        if (!empty($config['stringCertPath'] ?? '')) {
            $certEnvironment = new \mark\payment\kernel\CertEnvironment();
            $certEnvironment->certEnvironment($config['merchantCertPath'] ?? '', $config['stringCertPath'] ?? '', $config['stringRootCertPath'] ?? '');
            $config['merchantCertSN'] = $certEnvironment->getMerchantCertSN();
            $config['stringRootCertSN'] = $certEnvironment->getRootCertSN();
            $config['stringPublicKey'] = $certEnvironment->getCachedPublicKey();
        }

        $this->kernel = new \mark\payment\kernel\Kernel($config);
        self::$merchant = new Merchant($this->kernel);
        self::$trade = new Trade($this->kernel);
        self::$refund = new Refund($this->kernel);
        self::$settlement = new Settlement($this->kernel);
        self::$partner = new Partner($this->kernel);
        // self::$util = new Util($this->kernel);
    }

    /**
     * Config constructor.
     *
     * @param $config
     */
    public function construct($config = array()) {
        foreach ($config as $key => $item) {
            if (is_string($key)) {
                $keys = strtolower($key);
                $this->$keys = $item;
            } else {
                $this->$key = $item;
            }
        }
    }

    /**
     * @param array $config
     * @param bool  $origin
     *
     * @return static
     */
    public static function getInstance(array $config = array(), bool $origin = false): self {
        if (!(self::$instance instanceof self) || $origin) {
            self::$instance = new self($config);
        }

        return self::$instance;
    }

    /**
     * @param array $config
     *
     * @return \mark\payment\Merchant
     */
    public static function merchant($config = array()): Merchant {
        if (!(self::$merchant instanceof Merchant)) {
            self::getInstance($config);
        }

        return self::$merchant;
    }

    /**
     * @param array $config
     *
     * @return \mark\payment\Trade
     */
    public static function trade($config = array()): Trade {
        if (!(self::$trade instanceof Trade)) {
            self::getInstance($config);
        }

        return self::$trade;
    }

    /**
     * @param array $config
     *
     * @return \mark\payment\Refund
     */
    public static function refund($config = array()): Refund {
        if (!(self::$refund instanceof Refund)) {
            self::getInstance($config);
        }

        return self::$refund;
    }

    /**
     * @param array $config
     *
     * @return \mark\payment\Settlement
     */
    public static function settlement($config = array()): Settlement {
        if (!(self::$settlement instanceof Settlement)) {
            self::getInstance($config);
        }

        return self::$settlement;
    }

    /**
     * @param array $config
     *
     * @return \mark\payment\Partner
     */
    public static function partner($config = array()): Partner {
        if (!(self::$partner instanceof Partner)) {
            self::getInstance($config);
        }

        return self::$partner;
    }

    /**
     * 设置AppID
     *
     * @param mixed $appid
     *
     * @return $this
     */
    public function setAppID($appid): self {
        if (!empty($appid)) {
            $this->kernel->config['appid'] = $appid;
            $this->appid = $appid;
        }

        return $this;
    }

    /**
     * 设置企业ID
     *
     * @param mixed $corpid
     *
     * @return $this
     */
    public function setCorpid($corpid): self {
        if (!empty($corpid)) {
            $this->kernel->config['corpid'] = $corpid;
            $this->corpid = $corpid;
        }

        return $this;
    }

    /**
     * 设置商户ID
     *
     * @param mixed $mchid
     *
     * @return $this
     */
    public function setMchid($mchid): self {
        if (!empty($mchid)) {
            $this->kernel->config['mchid'] = $mchid;
            $this->mchid = $mchid;
        }

        return $this;
    }

    /**
     * 设置密钥
     *
     * @param string $secret
     *
     * @return $this
     */
    public function setSecret(string $secret): self {
        if (!empty($secret)) {
            $this->kernel->config['secret'] = $secret;
            $this->secret = $secret;
        }

        return $this;
    }

    /**
     * @param mixed $serial_no
     *
     * @return $this
     */
    public function setSerialNo($serial_no): self {
        if (!empty($serial_no)) {
            $this->kernel->config['serial_no'] = $serial_no;
            $this->serial_no = $serial_no;

            $this->kernel->headers['serial'] = $serial_no;
        }

        return $this;
    }

    /**
     * @param mixed $private_key
     *
     * @return $this
     */
    public function setPrivateKey($private_key): self {
        if (!empty($private_key)) {
            $this->kernel->config['private_key'] = $private_key;
            $this->private_key = $private_key;
        }

        return $this;
    }

    /**
     * @param mixed $certificate
     *
     * @return $this
     */
    public function setCertificate($certificate): self {
        if (!empty($certificate)) {
            $this->kernel->config['certificate'] = $certificate;
            $this->certificate = $certificate;
        }

        return $this;
    }

    /**
     * 设置临时票据
     *
     * @param string $ticket
     *
     * @return $this
     */
    public function setTicket(string $ticket): self {
        if (!empty($ticket)) {
            $this->kernel->config['ticket'] = $ticket;
            $this->ticket = $ticket;
        }

        return $this;
    }

    /**
     * 服务商模式
     *
     * @param $sp_appid
     * @param $sp_corpid
     * @param $sp_mchid
     *
     * @return $this
     */
    public function provider($sp_appid, $sp_corpid, $sp_mchid): self {
        if (!empty($sp_appid)) {
            $this->kernel->config['sp_appid'] = $sp_appid;
            $this->sp_appid = $sp_appid;
        }

        if (!empty($sp_corpid)) {
            $this->kernel->config['sp_corpid'] = $sp_corpid;
            $this->sp_corpid = $sp_corpid;
        }

        if (!empty($sp_mchid)) {
            $this->kernel->config['sp_mchid'] = $sp_mchid;
            $this->sp_mchid = $sp_mchid;
        }

        // $this->kernel->injectTextParam('app_auth_token', $appAuthToken);
        $this->kernel->config['pay_partner'] = 'provider';

        return $this;
    }

    /**
     * 业务扩展参数
     *
     * @param array $param
     *
     * @return $this
     */
    public function setExtendParams($param = array()): self {
        $this->kernel->options = array_merge_recursive($this->kernel->options, $param);

        return $this;
    }

    /**
     * @param bool $cache
     *
     * @return $this
     */
    public function setCache($cache = true): self {
        $this->kernel->config['cache'] = is_true($cache);

        return $this;
    }

}