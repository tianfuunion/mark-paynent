<?php
declare (strict_types=1);

namespace mark\payment\trade;

/**
 *
 * 订单操作接口（Interface OrderOpertor）
 *
 * @package mark\payment\trade
 */
interface OrderOpertor {

    /**
     * create new order
     *
     * @param $order
     *
     * @return mixed
     */
    function createNewOrder($order);

    /**
     * make the order payed
     *
     * @param $order
     *
     * @return mixed
     */
    function orderPayed($order);

    /**
     * make the order delivery
     *
     * @param $order
     *
     * @return mixed
     */
    function orderDelivery($order);

    /**
     * make the order reveived
     *
     * @param $order
     *
     * @return mixed
     */
    function orderReceived($order);

    /**
     * make the order complete
     *
     * @param $order
     *
     * @return mixed
     */
    function orderComplete($order);

}