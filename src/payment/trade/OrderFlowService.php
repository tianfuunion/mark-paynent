<?php
declare (strict_types=1);

namespace mark\payment\trade;

/**
 * Class OrderFlowService
 *
 * @package mark\payment\trade
 */
class OrderFlowService implements OrderOpertor {

    /**
     * 单一入口，统一管理状态变更
     *
     * @param OrderOpertor $opertor
     * @param              $state
     * @param              $data
     *
     * @return bool|mixed
     */
    public static function changeState(OrderOpertor $opertor, $state, $data): bool {
        // do some validation before return
        return self::dispatch($opertor, $state, $data);
    }

    /**
     * 状态机分发处理
     *
     * @param OrderOpertor $opertor
     * @param              $state
     * @param              $data
     *
     * @return bool|mixed
     */
    private static function dispatch(OrderOpertor $opertor, $state, $data): bool {
        switch ($state) {
            case OrderState::ORDER_STATUS_NEW:
                $result = $opertor->createNewOrder($data);
                break;
            case OrderState::ORDER_STATUS_PAYED:
                $result = $opertor->orderPayed($data);
                break;
            case OrderState::ORDER_STATUS_DELIVERY:
                $result = $opertor->orderDelivery($data);
                break;
            case OrderState::ORDER_STATUS_RECEIVED:
                $result = $opertor->orderReceived($data);
                break;
            case OrderState::ORDER_STATUS_COMPLETE:
                $result = $opertor->orderComplete($data);
                break;
            default:
                $result = false;
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    function createNewOrder($order) {
        // TODO: Implement createNewOrder() method.
    }

    /**
     * @inheritDoc
     */
    function orderPayed($order) {
        // TODO: Implement orderPayed() method.
    }

    /**
     * @inheritDoc
     */
    function orderDelivery($order) {
        // TODO: Implement orderDelivery() method.
    }

    /**
     * @inheritDoc
     */
    function orderReceived($order) {
        // TODO: Implement orderReceived() method.
    }

    /**
     * @inheritDoc
     */
    function orderComplete($order) {
        // TODO: Implement orderComplete() method.
    }
}