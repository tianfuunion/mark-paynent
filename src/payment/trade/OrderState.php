<?php
declare (strict_types=1);

namespace mark\payment\trade;

/**
 * Class OrderState
 *
 * @package mark\payment\trade
 */
final class OrderState {

    // 定义常量，订单的五种状态
    const ORDER_STATUS_NEW = 'new';
    const ORDER_STATUS_PAYED = 'payed';
    const ORDER_STATUS_DELIVERY = 'delivery';
    const ORDER_STATUS_RECEIVED = 'received';
    const ORDER_STATUS_COMPLETE = 'complete';
    const ORDER_STATUS_CLOSED = 'closed';

    /**
     * 交易订单状态列表
     *
     * @var array[]
     */
    public static $status = array(
        array('id' => 1, 'title' => '等待买家付款', 'name' => 'Wait_Buyer_Pay', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 2, 'title' => '等待卖家发货', 'name' => 'Wait_Seller_Send_Goods', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 3, 'title' => '卖家部分发货', 'name' => 'Seller_Consigned_Part', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 4, 'title' => '等待买家确认收货', 'name' => 'Wait_Buyer_Confirm_Goods', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 5, 'title' => '买家已签收', 'name' => 'Trade_Buyer_Signed', 'icon' => '', 'style' => '', 'describe' => '（货到付款专用）'),
        array('id' => 6, 'title' => '交易成功', 'name' => 'Trade_Finished', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 8, 'title' => '没有创建外部交易', 'name' => 'Trade_NO_CREATE_PAY', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 9, 'title' => '等待预授权确认', 'name' => 'Wait_Pre_Auth_Confirm', 'icon' => '', 'style' => '', 'describe' => '元购合约中'),
        array('id' => 10, 'title' => '交易关闭', 'name' => 'Trade_Closed', 'icon' => '', 'style' => '', 'describe' => '交易被买家、卖家、平台关闭'),
        array('id' => 11, 'title' => '外卡支付付款确认中', 'name' => 'Pay_Pending', 'icon' => '', 'style' => '', 'describe' => ''),
        array('id' => 14, 'title' => '该状态代表订单已付款但是处于禁止发货状态。', 'name' => 'Paid_Forbid_Consign', 'icon' => '', 'style' => '', 'describe' => ''),
    );

    public static $operates = array(
        array('id' => 0, 'title' => '临时订单', 'name' => 'Advance'),
        array('id' => 1, 'title' => '创建订单', 'name' => 'Create'),
        array('id' => 2, 'title' => '支付款项', 'name' => 'Paid'),
        array('id' => 3, 'title' => '已接单', 'name' => 'Received'),
        array('id' => 4, 'title' => '已取消', 'name' => 'Cancelled'),
        array('id' => 5, 'title' => '已发货', 'name' => 'Delivered'),
        array('id' => 6, 'title' => '已收货', 'name' => 'Received'),
        array('id' => 6, 'title' => '已确认', 'name' => 'Confirmed'),
        array('id' => 7, 'title' => '已评价', 'name' => 'Evaluated'),
        array('id' => 7, 'title' => '核销订单', 'name' => 'Verify'),
        array('id' => 8, 'title' => '完成订单', 'name' => 'Complete'),
        array('id' => 9, 'title' => '售后维权', 'name' => 'Safeguarding'),
        array('id' => 10, 'title' => '关闭订单', 'name' => 'CloseOrder'),
    );

    private $state;

    /**
     * @param $state
     *
     * @return $this
     */
    public function setLiftState($state): self {
        $this->state = $state;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLiftState() {
        return $this->state;
    }
}