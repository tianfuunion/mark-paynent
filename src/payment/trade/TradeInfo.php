<?php
declare (strict_types=1);

namespace mark\payment\trade;

/**
 * Class TradeInfo
 *
 * @package mark\payment\trade
 */
final class TradeInfo {
    private $trade_info = array();

    /**
     * TradeInfo constructor.
     *
     * @param string $appid        服务商公众号ID string[1,32]    是    body 服务商申请的公众号或移动应用appid。示例值：wx8888888888888888
     * @param string $mchid        服务商户号    string[1,32]    是    body 服务商户号，由微信支付生成并下发，示例值：1230000109
     * @param string $subject      订单标题 string[1,127]    是    body 商品描述，示例值：Image形象店-深圳腾大-QQ公仔
     * @param int    $amount       总金额    total    int    是    订单总金额，单位为分。
     * @param string $currency     货币类型    currency    string[1,16]    否    CNY：人民币，境内商户号仅支持人民币。    示例值：CNY
     * @param string $time_expire  交易结束时间   string[1,64]    否   body 订单失效时间
     *                             遵循rfc3339标准格式，格式为YYYY-MM-DDTHH:mm:ss+TIMEZONE，
     *                             YYYY-MM-DD表示年月日，T出现在字符串中，表示time元素的开头，HH:mm:ss表示时分秒，TIMEZONE表示时区（+08:00表示东八区时间，领先UTC 8小时，即北京时间）。
     *                             例如：2015-05-20T13:29:35+08:00表示，北京时间2015年5月20日 13点29分35秒。
     *                             示例值：2018-06-08T10:34:56+08:00
     * @param string $attach       附加数据 string[1,128] 否 body 附加数据，在查询API和支付通知中原样返回，可作为自定义参数使用,示例值：自定义数据
     * @param string $notify_url   通知地址 string[1,256] 是 body 通知URL必须为直接可访问的URL，不允许携带查询串。
     *                             格式：URL,示例值：https://www.weixin.qq.com/wxpay/pay.php
     * @param string $goods_tag    订单优惠标记        string[1,32]    否    body 订单优惠标记,示例值：WXG
     * @param array  $options
     */
    public function __construct(array $options = array()) {
        if (!empty($options['appid'] ?? '')) {
            $this->trade_info['appid'] = $options['appid'];
        }
        if (!empty($options['corpid'] ?? '')) {
            $this->trade_info['corpid'] = $options['corpid'];
        }
        if (!empty($options['mchid'] ?? '')) {
            $this->trade_info['mchid'] = $options['mchid'];
        }

        if (!empty($options['trade_no'] ?? '')) {
            $this->trade_info['trade_no'] = $options['trade_no'];
        }
        if (!empty($options['order_no'] ?? '')) {
            $this->trade_info['order_no'] = $options['order_no'];
        }

        if (!empty($options['subject'] ?? '')) {
            $this->trade_info['subject'] = $options['subject'];
        }
        if (!empty($options['subtitle'] ?? '')) {
            $this->trade_info['subtitle'] = $options['subtitle'];
        }

        $this->setAmount((int)($options['amount'] ?? 0), (int)($options['total'] ?? 0), $options['currency'] ?? '');

        if (!empty($options['time_expire'] ?? '')) {
            if ($options['time_expire'] <= 7200) {
                $this->trade_info['time_expire'] = time() + $options['time_expire'];
            } elseif ($options['time_expire'] > time() + 60) {
                $this->trade_info['time_expire'] = $options['time_expire'];
            } else {
                $this->trade_info['time_expire'] = time() + 60;
            }
        }

        if (!empty($options['attach'] ?? '')) {
            $this->trade_info['attach'] = $options['attach'];
        }

        if (!empty($options['notify_url'] ?? '')) {
            $this->trade_info['notify_url'] = $options['notify_url'];
        }
        if (!empty($options['return_url'] ?? '')) {
            $this->trade_info['return_url'] = $options['return_url'];
        }

        if (!empty($options['tag'] ?? '')) {
            $this->trade_info['tag'] = $options['tag'];
        }

        if (!empty($options['goods_tag'] ?? '')) {
            $this->trade_info['goods_tag'] = $options['goods_tag'];
        }

        $this->setPayer($options['openid'] ?? '');

        if (!empty($options['message'] ?? '')) {
            $this->trade_info['message'] = $options['message'];
        }
    }

    /**
     * @param $key
     * @param $value
     *
     * @return $this
     */
    public function set($key, $value): self {
        $this->trade_info[$key] = $value;

        return $this;
    }

    /**
     * 订单金额
     *
     * @param int    $amount   支付金额
     * @param int    $total    订单总金额
     * @param string $currency 货币类型
     *
     * @return $this
     */
    public function setAmount(int $amount, int $total, string $currency = 'CNY'): self {
        if (!empty($amount) && $total > 0) {
            $this->trade_info['amount'] = $amount;
        }
        if (!empty($total) && $total > 0) {
            $this->trade_info['total'] = $total;
        } elseif (!empty($amount) && $amount > 0) {
            $this->trade_info['total'] = $amount;
        }

        $this->trade_info['currency'] = $currency ?: 'CNY';

        return $this;
    }

    /**
     * 支付者
     *
     * @param string $openid 用户服务标识    sp_openid    string[1,128]    是    用户在服务商appid下的唯一标识。* 示例值：oUpF8uMuAJO_M2pxb1Q9zNjWeS6o
     *                       用户子标识    sub_openid    string[1,128]    否    用户在子商户appid下的唯一标识。 * 示例值：oUpF8uMuAJO_M2pxb1Q9zNjWeS6o
     *
     * @return $this
     */
    public function setPayer(string $openid): self {
        if (!empty($openid)) {
            $this->trade_info['payer'] = array('openid' => $openid);
            $this->trade_info['openid'] = $openid;
        }

        return $this;
    }

    /**
     * 优惠功能
     *
     * @param int    $cost_price 1、商户侧一张小票订单可能被分多次支付，订单原价用于记录整张小票的交易金额。
     *                           2、当订单原价与支付金额不相等，则不享受优惠。
     *                           3、该字段主要用于防止同一张小票分多次支付，以享受多次优惠的情况，正常支付订单不必上传此参数。
     * @param string $invoice_id 商家小票ID
     *
     * @return $this
     */
    public function setDetail(int $cost_price, string $invoice_id = ''): self {
        $this->trade_info['detail'] = array('base_price' => $cost_price,
                                            'cost_price' => $cost_price,
                                            'invoice_id' => $invoice_id);

        return $this;
    }

    /**
     * 设置优惠信息
     *
     * @param int    $cost_price
     * @param string $invoice_id
     *
     * @return $this
     */
    public function setDiscount(int $cost_price, string $invoice_id = ''): self {
        $this->trade_info['discount'] = array(
            'base_price' => $cost_price,
            'cost_price' => $cost_price,
            'invoice_id' => $invoice_id
        );

        return $this;
    }

    /**
     * 订单包含的商品列表信息，json格式，其它说明详见：“商品明细说明”
     *
     * @param string $goods_id        String    必填    32    商户侧商品编码    string[1,32] 是 由半角的大小写字母、数字、中划线、下划线中的一种或几种组成。apple-01
     * @param string $title           String    必填    256    商品的实际名称 * 示例值：iPhoneX 256G
     * @param int    $price           Price    必填    9    商品单价，单位为分 * 示例值：828800
     * @param int    $quantity        Number    必填    10    商品数量 * 示例值：1
     * @param string $category        String    可选    24    商品类目    34543238
     * @param string $categories_tree String    可选    128    商品类目树，从商品类目根节点到叶子节点的类目id组成，类目id值使用|分割    124868003|126232002|126252004
     * @param string $description     String    可选    1000    商品描述信息    特价手机
     * @param string $show_url        String    可选    400    商品的展示地址
     *
     * @return $this
     */
    public function addGoodsDetail(
        string $goods_id, string $title, int $price, int $quantity = 1, string $category = '', string $categories_tree = '', string $description = '', string $show_url = ''
    ): self {
        $this->trade_info['goods_detail'][] = array('goods_id' => $goods_id,
                                                    'title' => $title,
                                                    'price' => $price,
                                                    'quantity' => $quantity,
                                                    'category' => $category,
                                                    'categories_tree' => $categories_tree,
                                                    'description' => $description,
                                                    'show_url' => $show_url);

        return $this;
    }

    /**
     * 设置支付场景
     * 参数名    变量    类型[长度限制]    必填    描述
     *
     * @param string $payer_client_ip 用户终端IP    string[1,45]  是   调用微信支付API的机器IP，支持IPv4和IPv6两种格式的IP地址。示例值：14.23.150.211
     * @param string $device_id       商户端设备号    string[1,32] 否  商户端设备号（门店号或收银设备ID）。示例值：013467007045764
     *
     * @return $this
     */
    public function setSceneInfo(string $payer_client_ip, string $device_id = ''): self {
        $this->trade_info['scene_info'] = array('payer_client_ip' => $payer_client_ip, 'device_id' => $device_id);

        return $this;
    }

    /**
     * 设置商户门店信息
     *
     * @param string $storeid   门店编号 string[1,32]   是 商户侧门店编号 * 示例值：0001
     * @param string $title     门店名称 string[1,256]  否 商户侧门店名称 * 示例值：腾讯大厦分店
     * @param string $area_code 地区编码 string[1,32]   否 地区编码，详细请见省市区编号对照表。 * 示例值：440305
     * @param string $address   详细地址 string[1,512]  否 详细的商户门店地址 * 示例值：广东省深圳市南山区科技中一道10000号
     *
     * @return $this
     */
    public function setStoreInfo(string $storeid, string $title = '', string $area_code = '', string $address = ''): self {
        $this->trade_info['store_info'] = array('storeid' => $storeid, 'title' => $title, 'area_code' => $area_code, 'address' => $address);

        return $this;
    }

    /**
     * 设置结算信息
     *
     * @param bool $profit_sharing 是否指定分账 bool    否    是否指定分账，枚举值：true：是 false：否 示例值：true
     * @param int  $subsidy_amount 补差金额 int64    否    SettleInfo.profit_sharing为true时，该金额才生效。注意：单笔订单最高补差金额为5000元，示例值：10
     *
     * @return $this
     */
    public function setSettleInfo($profit_sharing = false, int $subsidy_amount = 0): self {
        if ($profit_sharing) {
            $this->trade_info['settle_info'] = array('profit_sharing' => $profit_sharing, 'subsidy_amount' => $subsidy_amount);
        }

        return $this;
    }

    /**
     * 设置开票信息
     *
     * @description invoice_info    InvoiceInfo    可选        开票信息
     *
     * @param bool   $is_support_invoice 必选    5    该交易是否支持开票    true
     * @param string $title              必选    80    开票商户名称：商户品牌简称|商户门店简称    ABC|003
     * @param string $tax_num            必选    30    税号    1464888883494
     * @param string $details            必选    400    开票内容 * 注：json数组格式    [{'code':'100294400','name':'服饰','num':'2','sumPrice':'200.00','taxRate':'6%'}]
     *
     * @return $this
     */
    public function setInvoiceInfo(bool $is_support_invoice, string $title, string $tax_num, string $details): self {
        $this->trade_info['invoice_info'] = array('is_support_invoice' => $is_support_invoice, 'title' => $title, 'tax_num' => $tax_num, 'details' => $details);

        return $this;
    }

    /**
     * 获取订单信息
     *
     * @return array
     */
    public function getTradeInfo(): array {
        $trade = $this->trade_info;
        $trade['appid'] = $this->trade_info['appid'] ?? '';
        $trade['corpid'] = $this->trade_info['corpid'] ?? '';
        $trade['mchid'] = $this->trade_info['mchid'] ?? '';
        $trade['trade_no'] = $this->trade_info['trade_no'] ?? '';

        $trade['subject'] = $this->trade_info['subject'] ?? '';
        $trade['message'] = $this->trade_info['message'] ?? '';
        $trade['amount'] = $this->trade_info['amount'] ?? 0;
        $trade['total'] = $this->trade_info['total'] ?? $this->trade_info['amount'] ?? 0;
        $trade['currency'] = $this->trade_info['currency'] ?? 'CNY';

        if (!empty($this->trade_info['time_expire'] ?? '')) {
            $trade['time_expire'] = $this->trade_info['time_expire'] ?? 0;
        }
        if (!empty($this->trade_info['attach'] ?? '')) {
            $trade['attach'] = $this->trade_info['attach'] ?? '';
        }
        $trade['notify_url'] = $this->trade_info['notify_url'] ?? '';
        if (!empty($this->trade_info['return_url'] ?? '')) {
            $trade['return_url'] = $this->trade_info['return_url'] ?? '';
        }

        if (!empty($this->trade_info['payer'] ?? '')) {
            $trade['payer'] = $this->trade_info['payer'] ?? array();
        }
        if (!empty($this->trade_info['detail'] ?? '')) {
            $trade['detail'] = $this->trade_info['detail'] ?? array();
        }
        if (!empty($this->trade_info['discount'] ?? '')) {
            $trade['discount'] = $this->trade_info['discount'] ?? array();
        }

        if (!empty($this->trade_info['tag'] ?? '')) {
            $trade['tag'] = $this->trade_info['tag'] ?? '';
        }
        if (!empty($this->trade_info['goods_tag'] ?? '')) {
            $trade['goods_tag'] = $this->trade_info['goods_tag'] ?? '';
        }
        if (!empty($this->trade_info['goods_detail'] ?? '')) {
            $trade['goods_detail'] = $this->trade_info['goods_detail'] ?? array();
        }

        if (!empty($this->trade_info['scene_info'] ?? '')) {
            $trade['scene_info'] = $this->trade_info['scene_info'] ?? array();
        }
        if (!empty($this->trade_info['store_info'] ?? '')) {
            $trade['store_info'] = $this->trade_info['store_info'] ?? array();
        }
        if (!empty($this->trade_info['settle_info'] ?? '')) {
            $trade['settle_info'] = $this->trade_info['settle_info'] ?? array();
        }

        return $trade;
    }

    /**
     * 判读订单数据是否为空
     *
     * @return bool
     */
    public function isEmpty(): bool {
        return empty($this->trade_info);
    }

}