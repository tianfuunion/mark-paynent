<?php
declare (strict_types=1);

namespace mark\payment\trade;

/**
 * Class TradeEvent
 *
 * @package mark\payment\trade
 */
final class TradeEvent {
    private function __construct() { }

    public static $TRANSACTION_PAYING = 'transaction.paying';                // 支付中;用户支付中（付款码支付）
    public static $TRANSACTION_UNPAID = 'transaction.unpaid';                // 未支付; 交易创建，等待买家付款（Wait_Buyer_Pay NotPay）'
    public static $TRANSACTION_PAID = 'transaction.paid';                    // 支付中;支付进行中（保留状态）
    public static $TRANSACTION_CANCELLED = 'transaction.cancelled';          // 取消支付; 用户取消支付
    public static $TRANSACTION_PAYERROR = 'transaction.payerror';            // 支付失败; 其他原因，如银行返回失败
    public static $TRANSACTION_FAILURE = 'transaction.failure';              // 交易失败
    public static $TRANSACTION_DELETED = 'transaction.deleted';              // 交易已删除
    public static $TRANSACTION_REVOKED = 'transaction.revoked';              // 撤销支付''商家撤销支付
    public static $TRANSACTION_SUCCESS = 'transaction.success';              // 交易成功 * 支付成功; 商户签约的产品支持退款功能的前提下，买家付款成功（可退款）
    public static $TRANSACTION_FROZEN = 'transaction.frozen';                // 交易冻结; 交易存在争议，冻结中
    public static $TRANSACTION_FINISHED = 'transaction.finished';            // '交易完成''商户签约的产品不支持退款功能的前提下，买家付款成功；或者，商户签约的产品支持退款功能的前提下，交易已经成功并且已经超过可退款期限（不可退款）
    public static $TRANSACTION_REFUND = 'transaction.refund';                // '售后/维权/退款进行中
    public static $TRANSACTION_PARTIAL = 'transaction.partial';              // 已部分退款
    public static $TRANSACTION_FULLREFUND = 'transaction.fullrefund';        // 全额退款
    public static $TRANSACTION_CLOSED = 'transaction.closed';                // 交易关闭; 在指定时间段内未支付时关闭的交易或在交易完成全额退款成功时关闭的交易
    public static $TRANSACTION_ABNORMAL = 'transaction.abnormal';            // 支付异常，支付过程中发现商户的账户失效作废或者冻结了，导致支付失败')

    public static $REFUND_SUCCESS = 'refund.success';               // 退款成功
    public static $REFUND_PARTIALREFUND = 'refund.partial';         // 已部分退款
    public static $REFUND_FULLREFUND = 'refund.full';               // 已全额退款
    public static $REFUND_FAILURE = 'refund.failure';               // 退款失败
    public static $REFUND_CLOSED = 'refund.closed';                 // 退款关闭
    public static $REFUND_ABNORMAL = 'refund.abnormal';             // 退款异常

    private static $event_type = array(
        'transaction.success' => array('id' => 1, 'title' => '支付成功', 'name' => 'transaction.success', 'describe' => ''),
        'transaction.fullrefund' => array('id' => 1, 'title' => '全额退款', 'name' => 'transaction.fullrefund', 'describe' => ''),
        'close.order' => array('id' => 1, 'title' => '关闭订单', 'name' => 'close.order', 'describe' => ''),

        'refund.success' => array('id' => 1, 'title' => '退款成功通知', 'name' => 'refund.success', 'describe' => ''),
        'refund.abnormal' => array('id' => 1, 'title' => '退款异常通知', 'name' => 'refund.abnormal', 'describe' => ''),
        'refund.closed' => array('id' => 1, 'title' => '退款关闭通知', 'name' => 'refund.closed', 'describe' => '')
    );

    /**
     * 获取通知事件
     *
     * @param string $type
     *
     * @return array
     */
    public static function getEventType(string $type): array {
        if (empty($type)) {
            return array();
        }

        if (in_array(strtolower($type), self::$event_type)) {
            return self::$event_type[$type];
        }

        return array();
    }

    /**
     * 获取事件列表
     *
     * @return array[]
     */
    public static function getEventList(): array {
        return self::$event_type;
    }

}