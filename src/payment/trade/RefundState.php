<?php
declare (strict_types=1);

namespace mark\payment\trade;

/**
 * Class RefundState
 *
 * @package mark\payment\trade
 */
final class RefundState {

    // 定义常量，订单的五种状态
    const ORDER_STATUS_NEW = 'new';
    const ORDER_STATUS_PAYED = 'payed';
    const ORDER_STATUS_DELIVERY = 'delivery';
    const ORDER_STATUS_RECEIVED = 'received';
    const ORDER_STATUS_COMPLETE = 'complete';
    const ORDER_STATUS_CLOSED = 'closed';

    /**
     * 交易退款状态列表
     *
     * @note 注意：商户必须根据不同类型的业务通知，正确的进行不同的业务处理，并且过滤重复的通知结果数据。
     * 在的业务通知中，只有交易通知状态为Trade_Success（可退款）或Trade_Finished（不可退款）时，系统才会认定为买家付款成功，避免导致业务逻辑重复执行，甚至导致资金损失。
     * 一般情况下，商户只需按照交易状态的默认返回情况来进行处理即可 。
     * @todo 支付状态是否需要考虑退款审核机制
     * @var array[]
     */
    public static $status = array(
        0 => array('id' => 0, 'title' => '退款中', 'name' => 'Paying', 'icon' => 'icon-waiting', 'style' => 'btn-secondary', 'describe' => '用户支付中（付款码支付）'),
        1 => array('id' => 1, 'title' => '待退款', 'name' => 'Unpaid', 'icon' => 'icon-waiting', 'style' => 'btn-primary', 'describe' => '交易创建，等待买家付款（Wait_Buyer_Pay NotPay）'),
        2 => array('id' => 2, 'title' => '退款中', 'name' => 'Paid', 'icon' => 'icon-pay', 'style' => 'btn-secondary', 'describe' => '支付进行中（保留状态）'),
        3 => array('id' => 3, 'title' => '取消支付', 'name' => 'Cancelled', 'icon' => 'icon-forbidden', 'style' => 'btn-error', 'describe' => '用户取消支付'),
        4 => array('id' => 4, 'title' => '退款失败', 'name' => 'PayError', 'icon' => 'icon-protect-error', 'style' => 'btn-danger', 'describe' => '其他原因，如银行返回失败'),
        404 => array('id' => 404, 'title' => '退款已删除', 'name' => 'Deleted', 'icon' => 'icon-forbidden', 'style' => 'btn-danger', 'describe' => '交易已删除'),
        5 => array('id' => 5, 'title' => '撤销退款', 'name' => 'Revoked', 'icon' => 'icon-forbidden', 'style' => 'btn-danger', 'describe' => '商家撤销支付'),
        6 => array('id' => 6, 'title' => '退款成功', 'name' => 'Success', 'icon' => 'icon-success', 'style' => 'btn-success', 'describe' => '商户签约的产品支持退款功能的前提下，买家付款成功（可退款）'),
        7 => array('id' => 7, 'title' => '交易冻结', 'name' => 'Frozen', 'icon' => 'icon-question', 'style' => 'btn-info', 'describe' => '交易存在争议，冻结中'),
        8 => array('id' => 8, 'title' => '退款完成', 'name' => 'Finished', 'icon' => 'icon-success', 'style' => 'btn-success',
                   'describe' => '商户签约的产品不支持退款功能的前提下，买家付款成功；或者，商户签约的产品支持退款功能的前提下，交易已经成功并且已经超过可退款期限（不可退款）'),
        9 => array('id' => 9, 'title' => '转入退款', 'name' => 'Refund', 'icon' => 'icon-refund', 'style' => 'btn-warning', 'describe' => '售后/维权/退款进行中'),
        91 => array('id' => 91, 'title' => '部分退款', 'name' => 'PartialRefund', 'icon' => 'icon-refund', 'style' => 'btn-secondary', 'describe' => '部分已退款'),
        92 => array('id' => 92, 'title' => '全额退款', 'name' => 'FullRefund', 'icon' => 'icon-refund', 'style' => 'btn-secondary', 'describe' => '已全额退款'),
        10 => array('id' => 10, 'title' => '退款关闭', 'name' => 'Closed', 'icon' => 'icon-error', 'style' => 'btn-danger', 'describe' => '在指定时间段内未支付时关闭的交易或在交易完成全额退款成功时关闭的交易'),
        11 => array('id' => 11, 'title' => '退款异常', 'name' => 'Abnormal', 'icon' => 'icon-forbidden', 'style' => 'btn-error', 'describe' => '支付异常，支付过程中发现商户的账户失效作废或者冻结了，导致支付失败')
    );

    /**
     * 获取退款状态
     *
     * @param $state
     *
     * @return array
     */
    public static function getState($state): array {
        if (array_key_exists($state, self::$status)) {
            return self::$status[$state] ?? array();
        }

        return self::$status[array_key_exists($state, self::$status) ? $state : 11];
    }
}