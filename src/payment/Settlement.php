<?php
declare (strict_types=1);

namespace mark\payment;

use mark\payment\kernel\Kernel;
use mark\response\Response;

/**
 * Class Settlement
 *
 * @description 商户账户信息
 * @package     mark\payment
 */
class Settlement {

    /**
     * 银行账户类型
     *
     * @var array[]
     */
    public static $types = array(
        0 => array('typeid' => 0, 'title' => '个人账户', 'name' => 'personal'),
        1 => array('typeid' => 1, 'title' => '法人账户', 'name' => 'legalPerson'),
        2 => array('typeid' => 2, 'title' => '对公账户', 'name' => 'Corporate'),
        3 => array('typeid' => 3, 'title' => '合作账户', 'name' => 'Partner'));

    /**
     * @var \mark\payment\kernel\Kernel
     */
    private $kernel;

    /**
     * Trade constructor.
     *
     * @param $kernel
     */
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    /**
     *
     */
    public function applyment() {

    }

    /**
     * 获取支付账户信息
     *
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function getSettleInfo(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid AppID', '无效的AppID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:settlement:info:corpid:' . $this->kernel->getConfig('corpid', '') . ':mchid:' . $this->kernel->getConfig('mchid', '');

        return $this->kernel->get(Payment::$host . '/api.php/merchant/settlement', array('access_token' => $access_token));
    }

    /**
     * 常见银行列表
     *
     * @description  城市商业银行、农村商业银行、信用合作联社及其他银行，请选择“其他银行”
     *
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function getBankPicker(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid AppID', '无效的AppID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:bankpicker';

        return $this->kernel->get(Payment::$host . '/api.php/merchant/bankpicker', array('access_token' => $access_token));
    }
}