<?php

declare (strict_types=1);

namespace mark\payment\pay;

use Exception;

/**
 * 微信支付API异常类
 *
 * @author  widyhu
 * Class WxPayException
 * @package mark\payment\pay
 */
class WxPayException extends Exception {
    /**
     * @return string
     */
    public function errorMessage() {
        return $this->getMessage();
    }
}