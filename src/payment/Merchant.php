<?php
declare (strict_types=1);

namespace mark\payment;

use mark\payment\kernel\Kernel;
use mark\response\Response;

/**
 * Class Merchant
 *
 * @package mark\payment
 */
class Merchant {

    /**
     * @var \mark\payment\kernel\Kernel
     */
    private $kernel;

    /**
     * Trade constructor.
     *
     * @param $kernel
     */
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    /**
     * 查询商户信息
     *
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function info(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid AppID', '无效的AppID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:merchant:info:corpid:' . $this->kernel->getConfig('corpid', '') . ':mchid:' . $this->kernel->getConfig('mchid');

        return $this->kernel->get(Payment::$host . '/api.php/merchant/info', array('access_token' => $access_token));
    }

    /**
     * 查询商户列表
     *
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function list(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid AppID', '无效的AppID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:merchant:list:corpid:' . $this->kernel->getConfig('corpid', '');

        return $this->kernel->get(Payment::$host . '/api.php/merchant/list', array('access_token' => $access_token));
    }

    /**
     * 创建商户
     *
     * @param string $access_token
     * @param array  $merchant
     *
     * @return \mark\response\Response
     */
    public function create(string $access_token, array $merchant): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($merchant)) {
            return Response::create('', 412, 'Invalid Merchant Info', '无效的商户信息');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/update', $merchant);
    }

    /**
     * 更新商户信息
     *
     * @param string $access_token
     * @param array  $merchant
     *
     * @return \mark\response\Response
     */
    public function update(string $access_token, array $merchant): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($merchant)) {
            return Response::create('', 412, 'Invalid Merchant Info', '无效的商户信息');
        }
        return $this->kernel->post(Payment::$host . '/api.php/merchant/update', $merchant);
    }

    /**
     * 关闭商户
     *
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function close(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        return $this->kernel->post(Payment::$host . '/api.php/merchant/close');
    }

    /**
     * 删除商户
     *
     * @param string $access_token
     *
     * @return \mark\response\Response
     */
    public function delete(string $access_token): Response {
        if (empty($access_token)) {
            return Response::create('', 412, 'Invalid access_token', '无效的access_token');
        }
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        return $this->kernel->post(Payment::$host . '/api.php/merchant/delete');
    }

}