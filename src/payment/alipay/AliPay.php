<?php
declare (strict_types=1);

namespace mark\payment\alipay;

/**
 * Class AliPay
 *
 * @package mark\payment\alipay
 */
class AliPay {

    public function __construct() { }

    public function pay() { }

    public function notity() { }

}