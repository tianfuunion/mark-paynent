<?php

// This file is auto-generated, don't edit it. Thanks.
namespace mark\payment\common\Models;

use AlibabaCloud\Tea\Model;

/**
 * Class DataDataserviceBillDownloadurlQueryResponse
 *
 * @package mark\payment\common\Models
 */
class DataDataserviceBillDownloadurlQueryResponse extends Model {

    protected $_name = [
        'httpBody' => 'http_body',
        'code' => 'code',
        'msg' => 'msg',
        'subCode' => 'sub_code',
        'sub_msg' => 'sub_msg',
        'billDownloadUrl' => 'bill_download_url',
    ];

    public function validate() {
        Model::validateRequired('httpBody', $this->httpBody, true);
        Model::validateRequired('code', $this->code, true);
        Model::validateRequired('msg', $this->msg, true);
        Model::validateRequired('subCode', $this->subCode, true);
        Model::validateRequired('sub_msg', $this->sub_msg, true);
        Model::validateRequired('billDownloadUrl', $this->billDownloadUrl, true);
    }

    /**
     * @return array
     */
    public function toMap(): array {
        $res = [];
        if (null !== $this->httpBody) {
            $res['http_body'] = $this->httpBody;
        }
        if (null !== $this->code) {
            $res['code'] = $this->code;
        }
        if (null !== $this->msg) {
            $res['msg'] = $this->msg;
        }
        if (null !== $this->subCode) {
            $res['sub_code'] = $this->subCode;
        }
        if (null !== $this->sub_msg) {
            $res['sub_msg'] = $this->sub_msg;
        }
        if (null !== $this->billDownloadUrl) {
            $res['bill_download_url'] = $this->billDownloadUrl;
        }
        return $res;
    }

    /**
     * @param array $map
     *
     * @return DataDataserviceBillDownloadurlQueryResponse
     */
    public static function fromMap($map = []) {
        $model = new self();
        if (isset($map['http_body'])) {
            $model->httpBody = $map['http_body'];
        }
        if (isset($map['code'])) {
            $model->code = $map['code'];
        }
        if (isset($map['msg'])) {
            $model->msg = $map['msg'];
        }
        if (isset($map['sub_code'])) {
            $model->subCode = $map['sub_code'];
        }
        if (isset($map['sub_msg'])) {
            $model->sub_msg = $map['sub_msg'];
        }
        if (isset($map['bill_download_url'])) {
            $model->billDownloadUrl = $map['bill_download_url'];
        }

        return $model;
    }

    /**
     * @description 响应原始字符串
     * @var string
     */
    public $httpBody;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $msg;

    /**
     * @var string
     */
    public $subCode;

    /**
     * @var string
     */
    public $sub_msg;

    /**
     * @var string
     */
    public $billDownloadUrl;

}