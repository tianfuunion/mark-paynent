<?php

// This file is auto-generated, don't edit it. Thanks.
namespace mark\payment\common\Models;

use AlibabaCloud\Tea\Model;

/**
 * Class TradeCreateResponse
 *
 * @package mark\payment\common\Models
 */
class TradeCreateResponse extends Model {
    protected $_name = [
        'httpBody' => 'http_body',
        'code' => 'code',
        'msg' => 'msg',
        'subCode' => 'sub_code',
        'sub_msg' => 'sub_msg',
        'out_trade_no' => 'out_trade_no',
        'trade_no' => 'trade_no',
    ];

    public function validate() {
        Model::validateRequired('httpBody', $this->httpBody, true);
        Model::validateRequired('code', $this->code, true);
        Model::validateRequired('msg', $this->msg, true);
        Model::validateRequired('subCode', $this->subCode, true);
        Model::validateRequired('sub_msg', $this->sub_msg, true);
        Model::validateRequired('out_trade_no', $this->out_trade_no, true);
        Model::validateRequired('trade_no', $this->trade_no, true);
    }

    public function toMap() {
        $res = [];
        if (null !== $this->httpBody) {
            $res['http_body'] = $this->httpBody;
        }
        if (null !== $this->code) {
            $res['code'] = $this->code;
        }
        if (null !== $this->msg) {
            $res['msg'] = $this->msg;
        }
        if (null !== $this->subCode) {
            $res['sub_code'] = $this->subCode;
        }
        if (null !== $this->sub_msg) {
            $res['sub_msg'] = $this->sub_msg;
        }
        if (null !== $this->out_trade_no) {
            $res['out_trade_no'] = $this->out_trade_no;
        }
        if (null !== $this->trade_no) {
            $res['trade_no'] = $this->trade_no;
        }
        return $res;
    }

    /**
     * @param array $map
     *
     * @return TradeCreateResponse
     */
    public static function fromMap($map = []) {
        $model = new self();
        if (isset($map['http_body'])) {
            $model->httpBody = $map['http_body'];
        }
        if (isset($map['code'])) {
            $model->code = $map['code'];
        }
        if (isset($map['msg'])) {
            $model->msg = $map['msg'];
        }
        if (isset($map['sub_code'])) {
            $model->subCode = $map['sub_code'];
        }
        if (isset($map['sub_msg'])) {
            $model->sub_msg = $map['sub_msg'];
        }
        if (isset($map['out_trade_no'])) {
            $model->out_trade_no = $map['out_trade_no'];
        }
        if (isset($map['trade_no'])) {
            $model->trade_no = $map['trade_no'];
        }
        return $model;
    }

    /**
     * @description 响应原始字符串
     * @var string
     */
    public $httpBody;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $msg;

    /**
     * @var string
     */
    public $subCode;

    /**
     * @var string
     */
    public $sub_msg;

    /**
     * @var string
     */
    public $out_trade_no;

    /**
     * @var string
     */
    public $trade_no;

}