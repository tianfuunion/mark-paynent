<?php
declare (strict_types=1);

namespace mark\payment\common;

use AlibabaCloud\Tea\Exception\TeaError;
use AlibabaCloud\Tea\Exception\TeaUnableRetryError;
use AlibabaCloud\Tea\Request;
use AlibabaCloud\Tea\Tea;
use Exception;
use mark\payment\common\Models\DataDataserviceBillDownloadurlQueryResponse;
use mark\payment\common\Models\TradeCancelResponse;
use mark\payment\common\Models\TradeCloseResponse;
use mark\payment\common\Models\TradeCreateResponse;
use mark\payment\common\Models\TradeFastpayRefundQueryResponse;
use mark\payment\common\Models\TradeQueryResponse;
use mark\payment\common\Models\TradeRefundResponse;

/**
 * Class Client
 *
 * @package mark\payment\common
 */
class Client {
    protected $_kernel;

    /**
     * Client constructor.
     *
     * @param $kernel
     */
    public function __construct($kernel) {
        $this->_kernel = $kernel;
    }

    /**
     * @param string $subject
     * @param string $out_trade_no
     * @param string $totalAmount
     * @param string $buyer_id
     *
     * @return TradeCreateResponse
     * @throws \AlibabaCloud\Tea\Exception\TeaError
     * @throws \Exception
     * @throws \AlibabaCloud\Tea\Exception\TeaUnableRetryError
     */
    public function create(string $subject, string $out_trade_no, string $totalAmount, string $buyer_id) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => [
                'maxAttempts' => 0
            ]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.trade.create',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = [
                    'subject' => $subject,
                    'out_trade_no' => $out_trade_no,
                    'total_amount' => $totalAmount,
                    'buyer_id' => $buyer_id
                ];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query =
                    $this->_kernel->sortMap(Tea::merge(['sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))],
                        $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.trade.create');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return TradeCreateResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return TradeCreateResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (\Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }
        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string $out_trade_no
     *
     * @return TradeQueryResponse
     * @throws TeaError
     * @throws Exception
     * @throws TeaUnableRetryError
     */
    public function query(string $out_trade_no) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => [
                'maxAttempts' => 0
            ]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.trade.query',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = [
                    'out_trade_no' => $out_trade_no
                ];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query = $this->_kernel->sortMap(Tea::merge([
                    'sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))
                ], $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.trade.query');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return TradeQueryResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return TradeQueryResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }
        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string $out_trade_no
     * @param string $refundAmount
     *
     * @return TradeRefundResponse
     * @throws TeaError
     * @throws Exception
     * @throws TeaUnableRetryError
     */
    public function refund(string $out_trade_no, string $refundAmount) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => [
                'maxAttempts' => 0
            ]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.trade.refund',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = [
                    'out_trade_no' => $out_trade_no,
                    'refund_amount' => $refundAmount
                ];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query = $this->_kernel->sortMap(Tea::merge([
                    'sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))
                ], $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.trade.refund');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return TradeRefundResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return TradeRefundResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }
        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string $out_trade_no
     *
     * @return TradeCloseResponse
     * @throws TeaError
     * @throws Exception
     * @throws TeaUnableRetryError
     */
    public function close($out_trade_no) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => [
                'maxAttempts' => 0
            ]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.trade.close',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = ['out_trade_no' => $out_trade_no];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query = $this->_kernel->sortMap(Tea::merge([
                    'sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))
                ], $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.trade.close');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return TradeCloseResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return TradeCloseResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }
        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string $out_trade_no
     *
     * @return TradeCancelResponse
     * @throws TeaError
     * @throws Exception
     * @throws TeaUnableRetryError
     */
    public function cancel($out_trade_no) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => ['maxAttempts' => 0]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.trade.cancel',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = [
                    'out_trade_no' => $out_trade_no
                ];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query = $this->_kernel->sortMap(Tea::merge([
                    'sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))
                ], $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.trade.cancel');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return TradeCancelResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return TradeCancelResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }
        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string $out_trade_no
     * @param string $outRequestNo
     *
     * @return TradeFastpayRefundQueryResponse
     * @throws TeaError
     * @throws Exception
     * @throws TeaUnableRetryError
     */
    public function queryRefund(string $out_trade_no, string $outRequestNo) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => [
                'maxAttempts' => 0
            ]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.trade.fastpay.refund.query',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = [
                    'out_trade_no' => $out_trade_no,
                    'out_request_no' => $outRequestNo
                ];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query = $this->_kernel->sortMap(Tea::merge([
                    'sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))
                ], $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.trade.fastpay.refund.query');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return TradeFastpayRefundQueryResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return TradeFastpayRefundQueryResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }

        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string $billType
     * @param string $billDate
     *
     * @return DataDataserviceBillDownloadurlQueryResponse
     * @throws TeaError
     * @throws Exception
     * @throws TeaUnableRetryError
     */
    public function downloadBill(string $billType, string $billDate) {
        $_runtime = [
            'ignoreSSL' => $this->_kernel->getConfig('ignoreSSL'),
            'httpProxy' => $this->_kernel->getConfig('httpProxy'),
            'connectTimeout' => 15000,
            'readTimeout' => 15000,
            'retry' => [
                'maxAttempts' => 0
            ]
        ];
        $_lastRequest = null;
        $_lastException = null;
        $_now = time();
        $_retryTimes = 0;
        while (Tea::allowRetry(@$_runtime['retry'], $_retryTimes, $_now)) {
            if ($_retryTimes > 0) {
                $_backoffTime = Tea::getBackoffTime(@$_runtime['backoff'], $_retryTimes);
                if ($_backoffTime > 0) {
                    Tea::sleep($_backoffTime);
                }
            }
            $_retryTimes = $_retryTimes + 1;
            try {
                $_request = new Request();
                $systemParams = [
                    'method' => 'payment.data.dataservice.bill.downloadurl.query',
                    'app_id' => $this->_kernel->getConfig('appId'),
                    'timestamp' => $this->_kernel->getTimestamp(),
                    'format' => 'json',
                    'version' => '1.0',
                    'payment_sdk' => $this->_kernel->getSdkVersion(),
                    'charset' => 'UTF-8',
                    'sign_type' => $this->_kernel->getConfig('signType'),
                    'app_cert_sn' => $this->_kernel->getMerchantCertSN(),
                    'root_cert_sn' => $this->_kernel->getRootCertSN()
                ];
                $bizParams = [
                    'bill_type' => $billType,
                    'bill_date' => $billDate
                ];
                $textParams = [];
                $_request->protocol = $this->_kernel->getConfig('protocol');
                $_request->method = 'POST';
                $_request->pathname = '/gateway.do';
                $_request->headers = [
                    'host' => $this->_kernel->getConfig('gatewayHost'),
                    'content-type' => 'application/x-www-form-urlencoded;charset=utf-8'
                ];
                $_request->query = $this->_kernel->sortMap(Tea::merge([
                    'sign' => $this->_kernel->sign($systemParams, $bizParams, $textParams, $this->_kernel->getConfig('merchantPrivateKey'))
                ], $systemParams, $textParams));
                $_request->body = $this->_kernel->toUrlEncodedRequestBody($bizParams);
                $_lastRequest = $_request;
                $_response = Tea::send($_request, $_runtime);
                $respMap = $this->_kernel->readAsJson($_response, 'payment.data.dataservice.bill.downloadurl.query');
                if ($this->_kernel->isCertMode()) {
                    if ($this->_kernel->verify($respMap, $this->_kernel->extractPublicKey($this->_kernel->getCertSN($respMap)))) {
                        return DataDataserviceBillDownloadurlQueryResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                } else {
                    if ($this->_kernel->verify($respMap, $this->_kernel->getConfig('PublicKey'))) {
                        return DataDataserviceBillDownloadurlQueryResponse::fromMap($this->_kernel->toRespModel($respMap));
                    }
                }
                throw new TeaError(['message' => '验签失败，请检查string公钥设置是否正确。']);
            } catch (Exception $e) {
                if (!($e instanceof TeaError)) {
                    $e = new TeaError([], $e->getMessage(), $e->getCode(), $e);
                }
                if (Tea::isRetryable($e)) {
                    $_lastException = $e;
                    continue;
                }
                throw $e;
            }
        }
        throw new TeaUnableRetryError($_lastRequest, $_lastException);
    }

    /**
     * @param string[] $parameters
     *
     * @return bool
     */
    public function verifyNotify($parameters = array()) {
        if ($this->_kernel->isCertMode()) {
            return $this->_kernel->verifyParams($parameters, $this->_kernel->extractPublicKey(''));
        } else {
            return $this->_kernel->verifyParams($parameters, $this->_kernel->getConfig('PublicKey'));
        }
    }

    /**
     * ISV代商户代用，指定appAuthToken
     *
     * @param string $appAuthToken 代调用token
     *
     * @return $this 本客户端，便于链式调用
     */
    public function agent(string $appAuthToken) {
        $this->_kernel->injectTextParam('app_auth_token', $appAuthToken);
        return $this;
    }

    /**
     * 用户授权调用，指定authToken
     *
     * @param string $authToken 用户授权token
     *
     * @return $this
     */
    public function auth(string $authToken) {
        $this->_kernel->injectTextParam('auth_token', $authToken);

        return $this;
    }

    /**
     * 设置异步通知回调地址，此处设置将在本调用中覆盖Config中的全局配置
     *
     * @param string $url 异步通知回调地址，例如：https://www.test.com/callback
     *
     * @return $this
     */
    public function asyncNotify(string $url) {
        $this->_kernel->injectTextParam('notify_url', $url);

        return $this;
    }

    /**
     * 将本次调用强制路由到后端系统的测试地址上，常用于线下环境内外联调，沙箱与线上环境设置无效
     *
     * @param string $testUrl 后端系统测试地址
     *
     * @return $this
     */
    public function route($testUrl) {
        $this->_kernel->injectTextParam('ws_service_url', $testUrl);

        return $this;
    }

    /**
     * 设置API入参中没有的其他可选业务请求参数(biz_content下的字段)
     *
     * @param string $key   业务请求参数名称（biz_content下的字段名，比如timeout_express）
     * @param object $value 业务请求参数的值，一个可以序列化成JSON的对象
     *                      如果该字段是一个字符串类型（String、Price、Date在SDK中都是字符串），请使用String储存
     *                      如果该字段是一个数值型类型（比如：Number），请使用Long储存
     *                      如果该字段是一个复杂类型，请使用嵌套的array指定各下级字段的值
     *                      如果该字段是一个数组，请使用array储存各个值
     *
     * @return $this
     */
    public function optional($key, $value) {
        $this->_kernel->injectBizParam($key, $value);

        return $this;
    }

    /**
     * 批量设置API入参中没有的其他可选业务请求参数(biz_content下的字段)
     * optional方法的批量版本
     *
     * @param array $optionalArgs 可选参数集合，每个参数由key和value组成，key和value的格式请参见optional方法的注释
     *
     * @return $this
     */
    public function batchOptional($optionalArgs = array()) {
        foreach ($optionalArgs as $key => $value) {
            $this->_kernel->injectBizParam($key, $value);
        }

        return $this;
    }

}