<?php
declare (strict_types=1);

namespace mark\payment;

use mark\payment\kernel\Kernel;
use mark\response\Response;

/**
 * Class Partner
 *
 * @todo         待完善
 * @modifyTime   2022:05:11 10:24:00
 * @author       Mark Young
 * @version      1.0.0
 * @description  服务商（银行、支付机构、电商平台不可用）使用该接口提交商家资料，帮助商家入驻成为微信支付的特约商户。
 * @package      mark\payment
 */
class Partner {

    private $kernel;

    /**
     * Trade constructor.
     *
     * @param $kernel
     */
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    /**
     * 查询服务商详情
     *
     * @return \mark\response\Response
     */
    public function info(): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            // return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('sp_mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的服务商ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:partner:query:appid:' . $this->kernel->getConfig('appid');

        return $this->kernel->get(Payment::$host . '/api.php/partner/info');
    }

    /**
     * 服务商（银行、支付机构、电商平台不可用）使用该接口提交商家资料，帮助商家入驻成为微信支付的特约商户。
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function apply(array $options = array()): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }
        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }
        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }
        if (empty($options['apply_no'] ?? '')) {
            return Response::create('', 412, 'Invalid apply_no', '无效的申请编号');
        }
        if (empty($options['openid'] ?? '')) {
            return Response::create('', 412, 'Invalid Manager OpenId', '无效的管理员OpenID');
        }

        return $this->kernel->post(Payment::$host . '/api.php/partner/apply', $options);
    }

    /**
     * 查询申请单状态
     *
     * @param string $apply_no
     *
     * @return \mark\response\Response
     */
    public function query(string $apply_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            // return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('sp_mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:partner:query:apply_no:' . $apply_no;

        return $this->kernel->get(Payment::$host . '/api.php/partner/query', ['apply_no' => $apply_no]);
    }

    /**
     * 更新结算账号
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function update(array $options = array()): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }
        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }
        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }
        if (empty($options['apply_no'] ?? '')) {
            return Response::create('', 412, 'Invalid apply_no', '无效的申请编号');
        }
        if (empty($options['openid'] ?? '')) {
            return Response::create('', 412, 'Invalid Manager OpenId', '无效的管理员OpenID');
        }

        return $this->kernel->post(Payment::$host . '/api.php/partner/update', $options);
    }

    /**
     * 查询结算账户
     *
     * @return \mark\response\Response
     */
    public function settlement(): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:partner:settlement:appid:' . $this->kernel->getConfig('appid');

        return $this->kernel->get(Payment::$host . '/api.php/partner/settlement');
    }
}