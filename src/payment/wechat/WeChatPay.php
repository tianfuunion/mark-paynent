<?php

declare (strict_types=1);

namespace mark\payment\wechat;

/**
 * Class WeChatPay
 *
 * @package mark\payment\wechat
 */
class WeChatPay {
    /**
     *
     */
    public function __construct() {
    }

    public function payment() {
    }

    /**
     * 查询微信支付详情
     */
    public function query() {

    }

    /**
     * 申请微信退款
     */
    public function refund() {

    }

    /**
     * 查询微信退款
     */
    public function refund_query() {

    }

}