<?php
declare (strict_types=1);

namespace mark\payment;

use mark\payment\kernel\Kernel;
use mark\response\Response;

/**
 * Class Refund
 *
 * @package mark\payment
 */
final class Refund {

    /**
     * @var \mark\payment\kernel\Kernel
     */
    private $kernel;

    /**
     * Refund constructor.
     *
     * @param \mark\payment\kernel\Kernel $kernel
     */
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    /**
     * 查询退款信息
     *
     * @param string $refund_no
     *
     * @return \mark\response\Response
     */
    public function info(string $refund_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($refund_no)) {
            return Response::create('', 412, 'Invalid refund No', '无效的退款单号');
        }

        $this->kernel->config['cacheKey'] =
            'payment:sdk:refund:info:corpid:' . $this->kernel->getConfig('corpid', '') . ':mchid:' . $this->kernel->getConfig('mchid', '') . ':refund_no:' . $refund_no;

        return $this->kernel->get(Payment::$host . '/api.php/refund/info', array('refund_no' => $refund_no));
    }

    /**
     * 查询退款信息
     *
     * @param string $refund_no
     *
     * @return \mark\response\Response
     */
    public function query(string $refund_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($refund_no)) {
            return Response::create('', 412, 'Invalid refund No', '无效的退款单号');
        }

        $this->kernel->config['cacheKey'] =
            'payment:sdk:refund:query:corpid:' . $this->kernel->getConfig('corpid', '') . ':mchid:' . $this->kernel->getConfig('mchid', '') . ':refund_no:' . $refund_no;

        return $this->kernel->get(Payment::$host . '/api.php/refund/query', array('refund_no' => $refund_no));
    }

    /**
     * 查询退款列表
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function list(array $options = array()): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $cacheKey = 'payment:sdk:refund:list:corpid:' . $this->kernel->getConfig('corpid', '') . ':mchid:' . $this->kernel->getConfig('mchid', '');

        $param = [];
        if (!empty($options['starttime'] ?? '')) {
            $param['starttime'] = $options['starttime'];
            $cacheKey .= ':starttime:' . $options['starttime'];
        }

        if (!empty($options['endtime'] ?? '')) {
            $param['endtime'] = $options['endtime'];
            $cacheKey .= ':endtime:' . $options['endtime'];
        }

        if (!empty($options['status'] ?? '')) {
            $param['status'] = $options['status'];
            $cacheKey .= ':status:' . $options['status'];
        }

        $this->kernel->config['cacheKey'] = $cacheKey;

        return $this->kernel->get(Payment::$host . '/api.php/refund/list', $param);
    }

    /**
     * 申请退款(带审核机制)
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function apply(array $options = array()): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }
        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }
        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }

        if (!empty($options['tradeid'] ?? '') || !empty($options['trade_no'])) {
            return Response::create('', 412, '', '无效的交易单号');
        }
        if (empty($options['refund_no'] ?? '')) {
            return Response::create('', 412, '', '无效的退款单号');
        }
        if (empty($options['amount'] ?? '')) {
            return Response::create('', 401, '', '无效的退款金额');
        }
        if (empty($options['total'] ?? '')) {
            return Response::create('', 401, '', '无效的订单金额');
        }
        if (empty($options['currency'] ?? '')) {
            $options['currency'] = 'CNY';
        }

        if (empty($options)) {
            return Response::create('', 412, 'Invalid trade No', '无效的退款信息');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/apply', $options);
    }

    /**
     * 审核退款信息
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function verify(array $options = array()): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }
        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }
        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }

        if (empty($options)) {
            return Response::create('', 412, 'Invalid trade No', '无效的退款信息');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/create', $options);
    }

    /**
     * 统一收单交易退款接口
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function refund(array $options): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($options['refund_no'] ?? '')) {
            return Response::create('', 412, 'Invalid refund no', '无效的退款单号');
        }
        if (empty($options['amount'] ?? '') || $options['amount'] <= 0) {
            return Response::create('', 412, 'Invalid refund amount', '无效的退款金额');
        }
        if (empty($options['total'] ?? '') || $options['total'] <= 0) {
            return Response::create('', 412, 'Invalid trade total', '无效的订单金额');
        }
        if (bccomp((string)($options['amount'] ?? '-2'), (string)($options['total'] ?? '-1')) === 1) {
            return Response::create('', 412, 'Invalid trade total', '非法的退款金额');
        }

        $refund_info = array(
            'refund_no' => $options['refund_no'] ?? '',
            'amount' => $options['amount'] ?? 0,
            'total' => $options['total'] ?? 0,
            'currency' => $options['currency'] ?? 'CNY');

        if (!empty($options['trade_no'] ?? '')) {
            $refund_info['trade_no'] = $options['trade_no'];
        } elseif (empty($options['pay_id'] ?? '')) {
            $refund_info['pay_id'] = $options['pay_id'];
        } else {
            return Response::create('', 412, 'Invalid trade no', '无效的交易单号');
        }

        if (!empty($options['reason'] ?? '')) {
            $refund_info['reason'] = $options['reason'] ?? '';
        }
        if (!empty($options['notify_url'] ?? '')) {
            $refund_info['notify_url'] = $options['notify_url'] ?? '';
        }

        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }

        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }

        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }

        if (empty($refund_info)) {
            return Response::create('', 412, 'Invalid refund info', '无效的退款信息');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/refunds', $refund_info);
    }

    /**
     * 取消退款
     *
     * @param string $refund_no
     *
     * @return \mark\response\Response
     */
    public function cancel(string $refund_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($refund_no)) {
            return Response::create('', 412, 'Invalid Refund No', '无效的退款单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/cancel', array('refund_no' => $refund_no));
    }

    /**
     * 关闭退款
     *
     * @param string $refund_no
     *
     * @return \mark\response\Response
     */
    public function close(string $refund_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($refund_no)) {
            return Response::create('', 412, 'Invalid Refund No', '无效的退款单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/close', array('refund_no' => $refund_no));
    }

    /**
     * 移除退款
     *
     * @param string $refund_no
     *
     * @return \mark\response\Response
     */
    public function remove(string $refund_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($refund_no)) {
            return Response::create('', 412, 'Invalid refund No', '无效的订单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/remove', array('refund_no' => $refund_no));
    }

    /**
     * 删除订单退款
     *
     * @param string $refund_no
     *
     * @return \mark\response\Response
     */
    public function delete(string $refund_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($refund_no)) {
            return Response::create('', 412, 'Invalid refund No', '无效的订单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/refund/delete', array('refund_no' => $refund_no));
    }

}