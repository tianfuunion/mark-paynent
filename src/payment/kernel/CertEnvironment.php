<?php
declare (strict_types=1);

namespace mark\payment\kernel;

/**
 * Class CertEnvironment
 *
 * @package mark\payment\kernel
 */
class CertEnvironment {
    private $rootCertSN;

    private $merchantCertSN;

    private $cachedstringPublicKey;

    /**
     * 构造证书运行环境
     *
     * @param string $merchantCertPath   商户公钥证书路径
     * @param string $stringCertPath     string公钥证书路径
     * @param string $stringRootCertPath string根证书路径
     */
    public function certEnvironment(string $merchantCertPath, string $stringCertPath, string $stringRootCertPath) {
        if (empty($merchantCertPath) || empty($stringCertPath) || empty($stringRootCertPath)) {
            throw new \mark\http\exception\RuntimeException('证书参数merchantCertPath、stringCertPath或stringRootCertPath设置不完整。');
        }

        $antCertificationUtil = new \mark\payment\kernel\Util\AntCertificationUtil();
        $this->rootCertSN = $antCertificationUtil->getRootCertSN($stringRootCertPath);
        $this->merchantCertSN = $antCertificationUtil->getCertSN($merchantCertPath);
        $this->cachedstringPublicKey = $antCertificationUtil->getPublicKey($stringCertPath);
    }

    /**
     * @return mixed
     */
    public function getRootCertSN() {
        return $this->rootCertSN;
    }

    /**
     * @return mixed
     */
    public function getMerchantCertSN() {
        return $this->merchantCertSN;
    }

    /**
     * @return mixed
     */
    public function getCachedPublicKey() {
        return $this->cachedstringPublicKey;
    }

}