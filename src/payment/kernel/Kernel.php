<?php
declare (strict_types=1);

namespace mark\payment\kernel;

use AlibabaCloud\Tea\FileForm\FileForm;
use AlibabaCloud\Tea\FileForm\FileForm\FileField;
use GuzzleHttp\Psr7\Stream;
use mark\auth\Authorize;
use mark\http\Client\Client;
use mark\payment\kernel\Util\AES;
use mark\payment\kernel\Util\JsonUtil;
use mark\payment\kernel\Util\PageUtil;
use mark\payment\kernel\Util\SignContentExtractor;
use mark\payment\kernel\Util\Signer;
use mark\response\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Kernel
 *
 * @package mark\payment\kernel
 */
class Kernel {

    public $config = array();
    public $options = array();
    public $headers = array();

    private $optionalTextParams;

    private $optionalBizParams;

    private $textParams;

    private $bizParams;

    /**
     * Kernel constructor.
     *
     * @param $config
     */
    public function __construct(array $config) {
        $this->config = $config;
    }

    /**
     * 注入文本参数
     *
     * @param $key
     * @param $value
     */
    public function injectTextParam($key, $value) {
        if ($key != null) {
            $this->optionalTextParams[$key] = $value;
        }
    }

    /**
     * 注入Biz参数
     *
     * @param $key
     * @param $value
     */
    public function injectBizParam($key, $value) {
        if ($key != null) {
            $this->optionalBizParams[$key] = $value;
        }
    }

    /**
     * 获取时间戳，格式yyyy-MM-dd HH:mm:ss
     *
     * @return false|string 当前时间戳
     */
    public function getTimestamp() {
        return date('Y-m-d H:i:s');
    }

    /**
     * @param string $key
     * @param null   $default
     *
     * @return array|mixed|null
     */
    public function getConfig(string $key = '', $default = null) {
        if (empty($key)) {
            return $this->config;
        }

        return $this->config[$key] ?? $default;
    }

    /**
     * @return mixed
     */
    public function getSdkVersion() {
        return Constants::SDK_VERSION;
    }

    /**
     * 将业务参数和其他额外文本参数按www-form-urlencoded格式转换成HTTP Body中的字节数组，注意要做URL Encode
     *
     * @param array $bizParams 业务参数
     *
     * @return false|string|null
     */
    public function toUrlEncodedRequestBody($bizParams) {
        $sortedMap = $this->getSortedMap(null, $bizParams, null);
        if (empty($sortedMap)) {
            return null;
        }
        return $this->buildQueryString($sortedMap);
    }

    /**
     * 解析网关响应内容，同时将API的接口名称和响应原文插入到响应数组的method和body字段中
     *
     * @param ResponseInterface $response HTTP响应
     * @param string            $method   调用的OpenAPI的接口名称
     *
     * @return array    响应的结果
     */
    public function readAsJson(ResponseInterface $response, string $method) {
        $responseBody = (string)$response->getBody();
        $map = [];
        $map[Constants::BODY_FIELD] = $responseBody;
        $map[Constants::METHOD_FIELD] = $method;

        return $map;
    }

    /**
     * 生成随机分界符，用于multipart格式的HTTP请求Body的多个字段间的分隔
     *
     * @return string 随机分界符
     */
    public function getRandomBoundary(): string {
        return date('Y-m-d H:i:s') . '';
    }

    /**
     * 将其他额外文本参数和文件参数按multipart/form-data格式转换成HTTP Body中
     *
     * @param $textParams
     * @param $fileParams
     * @param $boundary
     *
     * @return false|string
     */
    public function toMultipartRequestBody($textParams, $fileParams, $boundary) {
        $this->textParams = $textParams;
        if ($textParams != null && $this->optionalTextParams != null) {
            $this->textParams = array_merge($textParams, $this->optionalTextParams);
        } elseif ($textParams == null) {
            $this->textParams = $this->optionalTextParams;
        }
        if (count($fileParams) > 0) {

            foreach ($fileParams as $key => $value) {
                $fileField = new FileField();
                $fileField->filename = $value;
                $fileField->contentType = 'multipart/form-data;charset=utf-8;boundary=' . $boundary;
                $fileField->content = new Stream(fopen($value, 'r'));
                $this->textParams[$key] = $fileField;
            }
        }
        $stream = FileForm::toFileForm($this->textParams, $boundary);

        do {
            $readLength = $stream->read(1024);
        } while (0 != $readLength);
        return $stream;
    }

    /**
     * 生成页面类请求所需URL或Form表单
     *
     * @param $method
     * @param $systemParams
     * @param $bizParams
     * @param $textParams
     * @param $sign
     *
     * @return string
     * @throws \Exception
     */
    public function generatePage($method, $systemParams, $bizParams, $textParams, $sign) {
        if ($method == Constants::GET) {
            //采集并排序所有参数
            $sortedMap = $this->getSortedMap($systemParams, $bizParams, $textParams);
            $sortedMap[Constants::SIGN_FIELD] = $sign;
            return $this->getGatewayServerUrl() . '?' . $this->buildQueryString($sortedMap);
        } elseif ($method == Constants::POST) {
            //采集并排序所有参数
            $sortedMap = $this->getSortedMap($systemParams, $this->bizParams, $this->textParams);
            $sortedMap[Constants::SIGN_FIELD] = $sign;
            $pageUtil = new PageUtil();
            return $pageUtil->buildForm($this->getGatewayServerUrl(), $sortedMap);
        } else {
            throw new \Exception('不支持' . $method);
        }
    }

    /**
     *  获取商户应用公钥证书序列号，从证书模式运行时环境对象中直接读取
     *
     * @return mixed 商户应用公钥证书序列号
     */
    public function getMerchantCertSN() {
        return $this->getConfig('merchantCertSN');
    }

    /**
     * 从响应Map中提取string公钥证书序列号
     *
     * @param array $respMap string 响应Map
     *
     * @return mixed   string公钥证书序列号
     */
    public function getCertSN(array $respMap) {
        if (!empty($this->getConfig('merchantCertSN'))) {
            $body = json_decode($respMap[Constants::BODY_FIELD]);
            return $body->cert_sn;
        }

        return '';
    }

    /**
     * 获取string根证书序列号，从证书模式运行时环境对象中直接读取
     *
     * @return mixed string根证书序列号
     */
    public function getRootCertSN() {
        return $this->getConfig('RootCertSN');
    }

    /**
     * 是否是证书模式
     *
     * @return mixed true：是；false：不是
     */
    public function isCertMode() {
        return $this->getConfig('merchantCertSN');
    }

    /**
     * @param $CertSN
     *
     * @return mixed
     */
    public function extractPublicKey($CertSN) {
        // PHP 版本只存储一个版本string公钥
        return $this->getConfig('PublicKey');
    }

    /**
     * 验证签名
     *
     * @param string $respMap   响应内容，可以从中提取出sign和body
     * @param string $PublicKey string公钥
     *
     * @return bool  true：验签通过；false：验签不通过
     * @throws \Exception
     */
    public function verify($respMap, $PublicKey) {
        // @todo 非法字符串偏移 Constants::BODY_FIELD
        $resp = json_decode($respMap[Constants::BODY_FIELD] ?? '', true);
        $sign = $resp[Constants::SIGN_FIELD];
        $signContentExtractor = new SignContentExtractor();
        $content = $signContentExtractor->getSignSourceData($respMap[Constants::BODY_FIELD], $respMap[Constants::METHOD_FIELD]);
        $signer = new Signer();
        return $signer->verify($content, $sign, $PublicKey);
    }

    /**
     * 计算签名，注意要去除key或value为null的键值对
     *
     * @param array  $systemParams 系统参数集合
     * @param array  $bizParams    业务参数集合
     * @param array  $textParams   其他额外文本参数集合
     * @param string $privateKey   私钥
     *
     * @return string 签名值的Base64串
     */
    public function sign($systemParams, $bizParams, $textParams, $privateKey) {
        $sortedMap = $this->getSortedMap($systemParams, $bizParams, $textParams);
        $data = $this->getSignContent($sortedMap);
        $sign = new Signer();
        return $sign->sign($data, $privateKey);
    }

    /**
     * AES加密
     *
     * @param $content
     * @param $encryptKey
     *
     * @return string
     * @throws \Exception
     */
    public function aesEncrypt($content, $encryptKey) {
        $aes = new AES();
        return $aes->aesEncrypt($content, $encryptKey);
    }

    /**
     * AES解密
     *
     * @param $content
     * @param $encryptKey
     *
     * @return false|string
     * @throws \Exception
     */
    public function aesDecrypt($content, $encryptKey) {
        $aes = new AES();
        return $aes->aesDecrypt($content, $encryptKey);
    }

    /**
     * 生成sdkExecute类请求所需URL
     *
     * @param $systemParams
     * @param $bizParams
     * @param $textParams
     * @param $sign
     *
     * @return string
     */
    public function generateOrderString($systemParams, $bizParams, $textParams, $sign) {
        //采集并排序所有参数
        $sortedMap = $this->getSortedMap($systemParams, $bizParams, $textParams);
        $sortedMap[Constants::SIGN_FIELD] = $sign;
        return http_build_query($sortedMap);
    }

    /**
     * @param $randomMap
     *
     * @return mixed
     */
    public function sortMap($randomMap) {
        return $randomMap;
    }

    /**
     *  从响应Map中提取返回值对象的Map，并将响应原文插入到body字段中
     *
     * @param $respMap  string 响应内容
     *
     * @return mixed
     */
    public function toRespModel($respMap) {
        // @todo 非法字符串偏移 Constants::BODY_FIELD
        $body = $respMap[Constants::BODY_FIELD];
        $methodName = $respMap[Constants::METHOD_FIELD];
        $responseNodeName = str_replace('.', '_', $methodName) . Constants::RESPONSE_SUFFIX;

        $model = json_decode($body, true);
        if (strpos($body, Constants::ERROR_RESPONSE)) {
            $result = $model[Constants::ERROR_RESPONSE];
            $result[Constants::BODY_FIELD] = $body;
        } else {
            $result = $model[$responseNodeName];
            $result[Constants::BODY_FIELD] = $body;
        }

        return $result;
    }

    /**
     * @param $parameters
     * @param $publicKey
     *
     * @return bool
     */
    public function verifyParams($parameters, $publicKey): bool {
        $sign = new Signer();
        return $sign->verifyParams($parameters, $publicKey);
    }

    /**
     * 字符串拼接
     *
     * @param $a
     * @param $b
     *
     * @return string 字符串a和b拼接后的字符串
     */
    public function concatStr($a, $b) {
        return $a . $b;
    }

    /**
     * @param array $sortedMap
     *
     * @return false|string
     */
    private function buildQueryString(array $sortedMap) {
        $requestUrl = null;
        foreach ($sortedMap as $sysParamKey => $sysParamValue) {
            $requestUrl .= "$sysParamKey=" . urlencode($this->characet($sysParamValue, Constants::DEFAULT_CHARSET)) . '&';
        }

        $requestUrl = substr($requestUrl, 0, -1);
        return $requestUrl;
    }

    /**
     * @param $systemParams
     * @param $bizParams
     * @param $textParams
     *
     * @return array
     */
    private function getSortedMap($systemParams, $bizParams, $textParams) {
        $this->textParams = $textParams;
        $this->bizParams = $bizParams;
        if ($textParams != null && $this->optionalTextParams != null) {
            $this->textParams = array_merge($textParams, $this->optionalTextParams);
        } elseif ($textParams == null) {
            $this->textParams = $this->optionalTextParams;
        }
        if ($bizParams != null && $this->optionalBizParams != null) {
            $this->bizParams = array_merge($bizParams, $this->optionalBizParams);
        } elseif ($bizParams == null) {
            $this->bizParams = $this->optionalBizParams;
        }
        $json = new JsonUtil();
        if ($this->bizParams != null) {
            $bizParams = $json->toJsonString($this->bizParams);
        }
        $sortedMap = $systemParams;
        if (!empty($bizParams)) {
            $sortedMap[Constants::BIZ_CONTENT_FIELD] = json_encode($bizParams, JSON_UNESCAPED_UNICODE);
        }
        if (!empty($this->textParams)) {
            if (!empty($sortedMap)) {
                $sortedMap = array_merge($sortedMap, $this->textParams);
            } else {
                $sortedMap = $this->textParams;
            }
        }
        if ($this->getConfig(Constants::NOTIFY_URL_CONFIG_KEY) != null) {
            $sortedMap[Constants::NOTIFY_URL_FIELD] = $this->getConfig(Constants::NOTIFY_URL_CONFIG_KEY);
        }
        return $sortedMap;
    }

    /**
     * 获取签名字符串
     *
     * @param $params
     *
     * @return string
     */
    private function getSignContent($params) {
        ksort($params);
        $stringToBeSigned = '';
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && '@' != substr($v, 0, 1)) {
                // 转换成目标字符集
                $v = $this->characet($v, Constants::DEFAULT_CHARSET);
                if ($i == 0) {
                    $stringToBeSigned .= "$k" . '=' . "$v";
                } else {
                    $stringToBeSigned .= '&' . "$k" . '=' . "$v";
                }
                $i++;
            }
        }
        unset ($k, $v);
        return $stringToBeSigned;
    }

    /**
     * @param $params
     */
    private function setNotifyUrl($params) {
        if ($this->getConfig(Constants::NOTIFY_URL_CONFIG_KEY) != null && $params(Constants::NOTIFY_URL_CONFIG_KEY) == null) {
            $params[Constants::NOTIFY_URL_CONFIG_KEY] = $this->getConfig(Constants::NOTIFY_URL_CONFIG_KEY);
        }
    }

    /**
     * @return string
     */
    private function getGatewayServerUrl() {
        return $this->getConfig(Constants::PROTOCOL_CONFIG_KEY) . '://' . $this->getConfig(Constants::HOST_CONFIG_KEY) . '/gateway.do';
    }

    /**
     * 校验$value是否非空
     *
     * @param $value
     *
     * @return bool if not set ,return true;if is null , return true;
     */
    function checkEmpty($value) {
        if (!isset($value)) {
            return true;
        }

        if ($value === null) {
            return true;
        }

        if (trim($value) === '') {
            return true;
        }

        return false;
    }

    /**
     * 转换字符集编码
     *
     * @param $data
     * @param $targetCharset
     *
     * @return string
     */
    function characet($data, $targetCharset) {
        if (!empty($data)) {
            $fileType = Constants::DEFAULT_CHARSET;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset, $fileType);
            }
        }

        return $data;
    }

    /**
     * 创建并发送HTTP请求。
     *
     * @param string $method
     * @param string $uri
     * @param array  $options
     *
     * @return Response
     */
    private function execute(string $method, string $uri, array $options = []): Response {
        if (empty($uri)) {
            return Response::create('', 412, 'Invalid request URL', '无效的请求地址');
        }

        if (empty($method)) {
            return Response::create('', 412, 'Invalid request method', '无效的请求方法');
        }

        if (empty($options)) {
            // 列表类请求可以不要单独设置参数
            // return Response::create('', 412, 'Invalid request param', '无效的请求参数');
        }

        $client = Client::getInstance()
                        ->setExpire(Authorize::getInstance()->getExpire())
                        ->setCacheHandle(Authorize::getInstance()->getCacheHandle())
                        ->setCache($this->getConfig('cache', true))
                        ->addHeader('pattern', Authorize::getInstance()->getPattern())
                        ->addHeader('auth-type', Authorize::$_type)
                        ->addHeader('auth-version', Authorize::$_version)
                        ->addHeader('pay-type', \mark\payment\Payment::$_type)
                        ->addHeader('pay-version', \mark\payment\Payment::$_version)
                        ->addHeader('cache', $this->getConfig('cache', true));

        if (!empty($this->getConfig('cacheKey'))) {
            $client->setCacheKey($this->getConfig('cacheKey'));
        }
        if (!empty($this->headers) && is_array($this->headers)) {
            foreach ($this->headers as $key => $item) {
                $client->addHeader($key, $item);
            }
        }

        if (!empty($this->getConfig('serial_no'))) {
            $client->addHeader('serial_no', $this->getConfig('serial_no'));
        }
        if (!empty($this->getConfig('private_key'))) {
            $client->addHeader('private_key', $this->getConfig('private_key'));
        }
        if (!empty($this->getConfig('certificate'))) {
            $client->addHeader('certificate', $this->getConfig('certificate'));
        }

        if (!empty($this->getConfig('appid'))) {
            $client->appendData('appid', $this->getConfig('appid'));
        }
        if (!empty($this->getConfig('sp_appid'))) {
            $client->appendData('sp_appid', $this->getConfig('sp_appid'));
        }

        if (!empty($this->getConfig('corpid'))) {
            $client->appendData('corpid', $this->getConfig('corpid'));
        }
        if (!empty($this->getConfig('sp_corpid'))) {
            $client->appendData('sp_corpid', $this->getConfig('sp_corpid'));
        }

        if (!empty($this->getConfig('mchid'))) {
            $client->appendData('mchid', $this->getConfig('mchid'));
        }
        if (!empty($this->getConfig('sp_mchid'))) {
            $client->appendData('sp_mchid', $this->getConfig('sp_mchid'));
        }

        $client->append(array_merge($this->options, $options));

        switch (strtolower($method)) {
            case 'get':
                $response = $client->get($uri, 'json');
                break;
            case 'post':
                $response = $client->post($uri, 'json');
                break;
            case 'put':
                $response = $client->put($uri);
                break;
            case 'download':
                // @todo 待完善，可用于下载账单
                $response = $client->download($uri, $savePath = '', $fileName = '', $suffix = '');
                break;
            case 'delete':
                $response = $client->delete($uri);
                break;
            default:
                return Response::create('', 405, 'Invalid request method', '非法的请求方法');
            // not break;
        }

        $this->client = $client;
        return $response->api_decode();
    }

    /**
     * @var Client
     */
    private $client;

    /**
     * @return \mark\http\Client\Client
     */
    public function getClient(): Client {
        return $this->client;
    }

    /**
     * 创建并发送Get请求
     *
     * @param string $uri
     * @param array  $options
     *
     * @return Response
     */
    public function get(string $uri, array $options = []): Response {
        return $this->execute('get', $uri, $options);
    }

    /**
     * 创建并发送Post请求
     *
     * @param string $uri
     * @param array  $options
     *
     * @return Response
     */
    public function post(string $uri, array $options = []): Response {
        return $this->execute('post', $uri, $options);
    }
}