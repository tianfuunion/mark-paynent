<?php
declare (strict_types=1);

namespace mark\payment\kernel\Util;

/**
 * Class ResponseChecker
 *
 * @package mark\payment\kernel\Util
 */
class ResponseChecker {

    /**
     * @param $response
     *
     * @return bool
     */
    public function success($response): bool {
        if (!empty($response->code) && $response->code == 10000) {
            return true;
        }

        if (empty($response->code) && empty($response->subCode)) {
            return true;
        }

        return false;
    }

}