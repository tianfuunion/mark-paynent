<?php
declare (strict_types=1);

namespace mark\payment\kernel\Util;

use mark\payment\kernel\Constants;

/**
 * Class SignContentExtractor
 *
 * @package mark\payment\kernel\Util
 */
class SignContentExtractor {
    private $RESPONSE_SUFFIX = '_response';
    private $ERROR_RESPONSE = 'error_response';

    /**
     * @param string $body   网关的整体响应字符串
     * @param string $method 本次调用的OpenAPI接口名称
     *
     * @return string   待验签的原文
     * @throws \Exception
     */
    public function getSignSourceData($body, $method): string {
        $rootNodeName = str_replace('.', '_', $method) . $this->RESPONSE_SUFFIX;
        $rootIndex = strpos($body, $rootNodeName);
        if ($rootIndex !== strrpos($body, $rootNodeName)) {
            throw new \Exception('检测到响应报文中有重复的' . $rootNodeName . ',验签失败。');
        }

        $errorIndex = strpos($body, $this->ERROR_RESPONSE);
        if ($rootIndex > 0) {
            return $this->parserJSONSource($body, $rootNodeName, $rootIndex);
        }

        if ($errorIndex > 0) {
            return $this->parserJSONSource($body, $this->ERROR_RESPONSE, $errorIndex);
        }

        return '';
    }

    /**
     * @param $responseContent
     * @param $nodeName
     * @param $nodeIndex
     *
     * @return string
     */
    function parserJSONSource($responseContent, $nodeName, $nodeIndex): string {
        $signDataStartIndex = $nodeIndex + strlen($nodeName) + 2;
        if (strrpos($responseContent, Constants::CERT_SN_FIELD)) {
            $signIndex = strrpos($responseContent, "\"" . Constants::CERT_SN_FIELD . "\"");
        } else {
            $signIndex = strrpos($responseContent, "\"" . Constants::SIGN_FIELD . "\"");
        }
        // 签名前-逗号
        $signDataEndIndex = $signIndex - 1;
        $indexLen = $signDataEndIndex - $signDataStartIndex;
        if ($indexLen < 0) {
            return '';
        }
        $substr = substr($responseContent, $signDataStartIndex, $indexLen);

        return $substr ? $substr : '';
    }
}