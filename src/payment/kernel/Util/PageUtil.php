<?php
declare (strict_types=1);

namespace mark\payment\kernel\Util;

/**
 * Class PageUtil
 *
 * @package mark\payment\kernel\Util
 */
class PageUtil {

    /**
     * @param $actionUrl
     * @param $parameters
     *
     * @return string
     */
    public function buildForm($actionUrl, $parameters): string {
        $sHtml = "<form id='submit' name='submit' action='" . $actionUrl . '?charset=' . trim(\mark\payment\kernel\Constants::DEFAULT_CHARSET) . "' method='POST'>";
        while (list ($key, $val) = $this->fun_adm_each($parameters)) {
            if (false === $this->checkEmpty($val)) {
                $val = str_replace("'", '&apos;', $val);
                //$val = str_replace("\"","&quot;",$val);
                $sHtml .= "<input type='hidden' name='" . $key . "' value='" . $val . "'/>";
            }
        }

        //submit按钮控件请不要含有name属性
        $sHtml = $sHtml . "<input type='submit' value='ok' style='display:none;''></form>";

        $sHtml = $sHtml . "<script>document.forms['submit'].submit();</script>";

        return $sHtml;
    }

    /**
     * @param $array
     *
     * @return array|false
     */
    protected function fun_adm_each(&$array) {
        $res = array();
        $key = key($array);
        if ($key !== null) {
            next($array);
            $res[1] = $res['value'] = $array[$key];
            $res[0] = $res['key'] = $key;
        } else {
            $res = false;
        }
        return $res;
    }

    /**
     * 校验$value是否非空
     * if not set , return true;
     * if is null , return true;
     *
     * @param $value
     *
     * @return bool
     */
    protected function checkEmpty($value): bool {
        if (!isset($value)) {
            return true;
        }

        if ($value === null) {
            return true;
        }

        if (trim($value) === '') {
            return true;
        }

        return false;
    }

}