<?php
declare (strict_types=1);

namespace mark\payment\kernel\Util;

/**
 * Class Encrypt
 *
 * @package mark\payment\kernel\Util
 */
class Encrypt {
    const AES_ALG = 'AES';
    const AES_CBC_PCK_ALG = 'AES/CBC/PKCS5Padding';
}