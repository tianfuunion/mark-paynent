<?php
declare (strict_types=1);

namespace mark\payment;

use mark\payment\kernel\Kernel;
use mark\payment\trade\TradeInfo;
use mark\response\Response;

/**
 * Class Trade
 *
 * @package mark\payment
 */
final class Trade {

    /**
     * @var \mark\payment\kernel\Kernel
     */
    private $kernel;

    /**
     * Trade constructor.
     *
     * @param \mark\payment\kernel\Kernel $kernel
     */
    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
    }

    /**
     * 统一收单线下交易预创建
     * 收银员通过收银台或商户后台调用string接口，生成二维码后，展示给用户，由用户扫描二维码完成订单支付。
     *
     * @param \mark\payment\trade\TradeInfo $trade
     *
     * @return \mark\response\Response
     */
    public function precreate(TradeInfo $trade): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }
        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }
        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }

        if ($trade->isEmpty()) {
            return Response::create('', 412, 'Invalid trade Info', '无效的订单信息');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/precreate', $trade->getTradeInfo());
    }

    /**
     * 统一收单交易创建接口
     * 除付款码支付场景以外，商户系统先调用该接口在微信支付服务后台生成预支付交易单，
     * 返回正确的预支付交易会话标识后再按Native、JSAPI、APP等不同场景生成交易串调起支付。
     *
     * @param \mark\payment\trade\TradeInfo $trade
     *
     * @return \mark\response\Response
     */
    public function create(TradeInfo $trade): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($this->kernel->getConfig('serial_no'))) {
            return Response::create('', 412, 'Invalid serial_no', '无效的序列号');
        }
        if (empty($this->kernel->getConfig('private_key'))) {
            return Response::create('', 412, 'Invalid private key', '无效的私钥');
        }
        if (empty($this->kernel->getConfig('certificate'))) {
            return Response::create('', 412, 'Invalid certificate', '无效的证书');
        }

        if ($trade->isEmpty()) {
            return Response::create('', 412, 'Invalid trade Info', '无效的订单信息');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/create', $trade->getTradeInfo());
    }

    /**
     * 查询订单信息
     *
     * @param string $trade_no
     *
     * @return \mark\response\Response
     */
    public function info(string $trade_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }
        if (empty($trade_no)) {
            return Response::create('', 412, 'Invalid trade No', '无效的订单号');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:trade:info:corpid:' . $this->kernel->getConfig('corpid', '')
            . ':mchid:' . $this->kernel->getConfig('mchid', '') . ':trade_no:' . $trade_no;

        return $this->kernel->get(Payment::$host . '/api.php/trade/info', array('trade_no' => $trade_no));
    }

    /**
     * 查询交易订单
     *
     * @param string $trade_no
     *
     * @return \mark\response\Response
     */
    public function query(string $trade_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($trade_no)) {
            return Response::create('', 412, 'Invalid trade No', '无效的订单号');
        }

        $this->kernel->config['cacheKey'] = 'payment:sdk:trade:query:corpid:' . $this->kernel->getConfig('corpid', '')
            . ':mchid:' . $this->kernel->getConfig('mchid', '') . ':trade_no:' . $trade_no;

        return $this->kernel->get(Payment::$host . '/api.php/trade/query', array('trade_no' => $trade_no));
    }

    /**
     * 查询交易列表
     *
     * @param array $options
     *
     * @return \mark\response\Response
     */
    public function list(array $options = []): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        $cacheKey = 'payment:sdk:trade:list:corpid:' . $this->kernel->getConfig('corpid', '') . ':mchid:' . $this->kernel->getConfig('mchid', '');

        $param = [];
        if (!empty($options['starttime'] ?? '')) {
            $param['starttime'] = $options['starttime'];
            $cacheKey .= ':starttime:' . $options['starttime'];
        }

        if (!empty($options['endtime'] ?? '')) {
            $param['endtime'] = $options['endtime'];
            $cacheKey .= ':endtime:' . $options['endtime'];
        }

        if (!empty($options['status'] ?? '')) {
            $param['status'] = $options['status'];
            $cacheKey .= ':status:' . $options['status'];
        }

        $this->kernel->config['cacheKey'] = $cacheKey;

        return $this->kernel->get(Payment::$host . '/api.php/trade/list', $param);
    }

    /**
     * 取消交易
     *
     * @param string $trade_no
     *
     * @return \mark\response\Response
     */
    public function cancel(string $trade_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($trade_no)) {
            return Response::create('', 412, 'Invalid trade No', '无效的订单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/cancel', array('trade_no' => $trade_no));
    }

    /**
     * 关闭交易
     *
     * @param string $trade_no
     *
     * @return \mark\response\Response
     */
    public function close(string $trade_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($trade_no)) {
            return Response::create('', 412, 'Invalid trade No', '无效的订单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/close', array('trade_no' => $trade_no));
    }

    /**
     * 移除交易
     *
     * @param string $trade_no
     *
     * @return \mark\response\Response
     */
    public function remove(string $trade_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($trade_no)) {
            return Response::create('', 412, 'Invalid trade No', '无效的订单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/remove', array('trade_no' => $trade_no));
    }

    /**
     * 删除交易
     *
     * @param string $trade_no
     *
     * @return \mark\response\Response
     */
    public function delete(string $trade_no): Response {
        if (empty($this->kernel->getConfig('appid'))) {
            return Response::create('', 412, 'Invalid APPID', '无效的APPID');
        }
        if (empty($this->kernel->getConfig('corpid'))) {
            return Response::create('', 412, 'Invalid Corporate ID', '无效的企业ID');
        }
        if (empty($this->kernel->getConfig('mchid'))) {
            return Response::create('', 412, 'Invalid Merchant ID', '无效的商户ID');
        }

        if (empty($trade_no)) {
            return Response::create('', 412, 'Invalid trade No', '无效的订单号');
        }

        return $this->kernel->post(Payment::$host . '/api.php/trade/delete', array('trade_no' => $trade_no));
    }

}